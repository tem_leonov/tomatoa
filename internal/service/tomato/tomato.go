package tomato

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/pkg/errors"
)

type IntervalType uint

const (
	WorkInterval  IntervalType = 1
	BreakInterval IntervalType = 2
)

type Storage interface {
	UpdateTomatoesCompleted(complete uint) error
	TomatoesCompleted() (uint, error)
	UpdateBreakTime(breakTime bool) error
	BreakTime() (bool, error)
}

type Service struct {
	storage Storage

	mux *sync.Mutex
}

func NewService(storage Storage) *Service {
	var mux sync.Mutex
	return &Service{storage: storage, mux: &mux}
}

func (s *Service) Start() (chan struct{}, time.Time, IntervalType) {
	s.mux.Lock()
	sig := make(chan struct{})
	dur, typ := s.defineInterval()
	end := time.Now().Add(dur)
	go func(mux *sync.Mutex, sc chan struct{}, e time.Time) {
		defer close(sc)
		defer mux.Unlock()
		s.start(sc, e)
	}(s.mux, sig, end)
	return sig, end, typ
}

func (s *Service) State() (uint, bool) {
	return s.tomatoesCompleted(), s.breakTime()
}

func (s *Service) ToNextState() (uint, bool, error) {
	completed, breakTime := s.State()
	newBreakTime := !breakTime
	err := s.storage.UpdateBreakTime(newBreakTime)
	if err != nil {
		return 0, false, errors.Wrap(err, "updating break time")
	}

	newCompleted := completed
	if !breakTime {
		newCompleted = completed + 1
	} else if completed == 4 {
		newCompleted = 0
	}
	if newCompleted != completed {
		err := s.storage.UpdateTomatoesCompleted(newCompleted)
		if err != nil {
			return 0, false, errors.Wrap(err, "updating tomatoes completed")
		}
	}

	return newCompleted, newBreakTime, nil
}

func (s *Service) ResetState() error {
	err := s.storage.UpdateBreakTime(false)
	if err != nil {
		return errors.Wrap(err, "updating break time")
	}

	err = s.storage.UpdateTomatoesCompleted(0)
	if err != nil {
		return errors.Wrap(err, "updating tomatoes completed")
	}

	return nil
}

func (s *Service) defineInterval() (time.Duration, IntervalType) {
	breakTime := s.breakTime()
	if !breakTime {
		return 25 * time.Minute, WorkInterval
	} else {
		completed := s.tomatoesCompleted()
		if completed != 4 {
			return 5 * time.Minute, BreakInterval
		} else {
			return 15 * time.Minute, BreakInterval
		}
	}
}

func (s *Service) tomatoesCompleted() uint {
	completed, err := s.storage.TomatoesCompleted()
	if err != nil {
		completed = 0
	}
	return completed
}

func (s *Service) breakTime() bool {
	breakTime, err := s.storage.BreakTime()
	if err != nil {
		breakTime = false
	}
	return breakTime
}

func (s *Service) start(sig chan struct{}, end time.Time) {
	s.startInterval(end, sig)
	_, _, err := s.ToNextState()
	if err != nil {
		log.Printf("updating state: %s", err)
	}
}

func (s *Service) startInterval(end time.Time, sig chan struct{}) {
	ctx, _ := context.WithDeadline(context.Background(), end)
	t := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ctx.Done():
			return
		case <-t.C:
			sig <- struct{}{}
		}
	}
}

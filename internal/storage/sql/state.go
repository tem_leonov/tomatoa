package sql

import (
	"database/sql"
	"fmt"
	"strconv"

	"github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
)

const (
	stateTable          = "state"
	keyColumn           = "key"
	valueColumn         = "value"
	tomatoesCompleteKey = "tomatoes_completed"
	breakTimeKey        = "break_time"
)

type stateVal struct {
	Key   string `db="key"`
	Value string `db="value"`
}

func (s *Storage) TomatoesCompleted() (uint, error) {
	q := squirrel.Select(valueColumn).
		From(stateTable).
		Where(squirrel.Eq{keyColumn: tomatoesCompleteKey})
	row := q.RunWith(s.db).QueryRow()
	var val string
	err := row.Scan(&val)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
		return 0, errors.Wrap(err, "executing query")
	}

	completed, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, "parsing value")
	}

	return uint(completed), nil
}

func (s *Storage) BreakTime() (bool, error) {
	q := squirrel.Select(valueColumn).
		From(stateTable).
		Where(squirrel.Eq{keyColumn: breakTimeKey})
	row := q.RunWith(s.db).QueryRow()
	var val string
	err := row.Scan(&val)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, errors.Wrap(err, "executing query")
	}

	breakTime, err := strconv.ParseBool(val)
	if err != nil {
		return false, errors.Wrap(err, "parsing value")
	}

	return breakTime, nil
}

func (s *Storage) UpdateTomatoesCompleted(complete uint) error {
	q := squirrel.Insert(stateTable).
		Columns(keyColumn, valueColumn).
		Values(tomatoesCompleteKey, strconv.FormatUint(uint64(complete), 10)).
		Suffix(
			fmt.Sprintf(
				`
			ON CONFLICT (%s) DO
			UPDATE
			SET %s = excluded.%s
		`,
				keyColumn,
				valueColumn, valueColumn,
			),
		)

	_, err := q.RunWith(s.db).Exec()
	if err != nil {
		return errors.Wrap(err, "executing query")
	}

	return nil
}

func (s *Storage) UpdateBreakTime(breakTime bool) error {
	q := squirrel.Insert(stateTable).
		Columns(keyColumn, valueColumn).
		Values(breakTimeKey, strconv.FormatBool(breakTime)).
		Suffix(
			fmt.Sprintf(
				`
			ON CONFLICT (%s) DO
			UPDATE
			SET %s = excluded.%s
		`,
				keyColumn,
				valueColumn, valueColumn,
			),
		)

	_, err := q.RunWith(s.db).Exec()
	if err != nil {
		return errors.Wrap(err, "executing query")
	}

	return nil
}

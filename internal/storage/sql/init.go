package sql

import (
	"fmt"

	"github.com/pkg/errors"
)

func (s *Storage) InitDatabase() error {
	q := fmt.Sprintf(
		`
	CREATE TABLE %s (
		%s TEXT PRIMARY KEY,
		%s TEXT
	);`,
		stateTable,
		keyColumn,
		valueColumn,
	)
	_, err := s.db.Exec(q)
	if err != nil {
		return errors.Wrap(err, "executing query")
	}
	return nil
}

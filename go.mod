module gitlab.com/tem_leonov/tomatoa

go 1.19

require (
	github.com/Masterminds/squirrel v1.5.3
	github.com/gen2brain/beeep v0.0.0-20220909211152-5a9ec94374f6
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/pkg/errors v0.9.1
)

require (
	github.com/go-toast/toast v0.0.0-20190211030409-01e6764cf0a4 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
)

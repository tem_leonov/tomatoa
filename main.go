package main

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"time"

	"github.com/gen2brain/beeep"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"

	"gitlab.com/tem_leonov/tomatoa/internal/service/tomato"
	sqlStorage "gitlab.com/tem_leonov/tomatoa/internal/storage/sql"
)

const ClearLine = "\033[2K"

func main() {
	service := initApp()

	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "next":
			completed, breakTime, err := service.ToNextState()
			if err != nil {
				log.Fatalf("updating state: %s", err)
			}

			fmt.Printf("NEW STATE %s", printState(completed, breakTime))
			return
		case "reset":
			err := service.ResetState()
			if err != nil {
				log.Fatalf("reseting state")
			}

			fmt.Printf("NEW STATE %s", printState(0, false))
			return
		default:
		}
	}

	fmt.Println(printState(service.State()))
	sig, end, typ := service.Start()
	fmt.Print(printTimer(end, typ))
	for range sig {
		fmt.Print(printTimer(end, typ))
	}
	beeep.Alert("Tomatoa", "Time is out", "")
}

func initApp() *tomato.Service {
	usr, err := user.Current()
	if err != nil {
		log.Fatalf("error getting current user: %s", err)
	}

	dbPath := fmt.Sprintf("%s/Library/tomatoa/tomatoa.db", usr.HomeDir)
	initRequired := false
	if _, err := os.Stat(dbPath); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			err := os.MkdirAll(filepath.Dir(dbPath), 0755)
			if err != nil {
				log.Fatalf("error creating db path")
			}

			_, err = os.Create(dbPath)
			if err != nil {
				log.Fatalf("error creating db file: %s", err)
			}
			initRequired = true
		} else {
			log.Fatalf("error checking db file: %s", err)
		}
	}

	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatalf("error opening db: %s", err)
	}

	storage := sqlStorage.NewStorage(db)

	if initRequired {
		storage.InitDatabase()
	}

	service := tomato.NewService(storage)
	return service
}

func printState(tomatoesCompleted uint, breakTime bool) string {
	const (
		CompletedTomatoSym    = "\u2713"
		NotCompletedTomatoSym = "\u25ef"
		BreakSym              = "\u266b"
	)
	syms := make([]string, 0, 5)
	for i := 0; i < int(tomatoesCompleted); i++ {
		syms = append(syms, CompletedTomatoSym)
	}
	if breakTime {
		syms = append(syms, BreakSym)
	}
	for i := tomatoesCompleted + 1; i <= 4; i++ {
		syms = append(syms, NotCompletedTomatoSym)
	}
	return fmt.Sprint(strings.Join(syms, " "))
}

func printTimer(end time.Time, typ tomato.IntervalType) string {
	var minutes, seconds uint
	if end.Before(time.Now()) {
		minutes = 0
		seconds = 0
	} else {
		left := end.Sub(time.Now())
		minutes = uint(math.Floor(left.Minutes()))
		seconds = uint(math.Floor(left.Seconds() - float64(minutes*60)))
	}

	fmt.Print(ClearLine)
	var prefix string
	switch typ {
	case tomato.WorkInterval:
		prefix = "WORK"
	case tomato.BreakInterval:
		prefix = "BREAK"
	}
	return fmt.Sprintf("\r%s %02d:%02d", prefix, minutes, seconds)
}
